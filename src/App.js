import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';

// Britt walked Jonny and I through this assessment to help us finish up.

class App extends Component {
  state = {
    todos: todosList,
    // value: ''
  };


  // {
  //     "userId": 1,
  //     "id": 1,
  //     "title": "delectus aut autem",
  //     "completed": false
  //   }



  handleAddTodo = (evt) => {
    if (evt.key === 'Enter') {
      const newTodo = {
        "userId": 1,
        "id": uuidv4(),
        "title": evt.target.value,
        "completed": false
      };
      const newTodos = this.state.todos.slice()
      newTodos.push(newTodo)
      console.log(newTodos)
      this.setState({ todos: newTodos })
      evt.target.value = ''

      // newTodos[todo.id] = todo;
    }
  }

  handleCompleteToDo = (id) => {
    let newTodos = this.state.todos.map(
      (todo) => {
        if (todo.id === id) {
          return { ...todo, completed: !todo.completed, }
      }
        return todo
      })
    this.setState({ todos: newTodos, })
    console.log('handle delete todo')
  }

  handleDelete = (todoId) => {
    let newTodos = this.state.todos.filter(
      (todo) => todo.id !== todoId)
      console.log('handle delete todo')
      this.setState({ todos: newTodos })
  }

  handleClearComplete = () => {
    let newCompleted = this.state.todos.filter(
      (todo) => todo.completed === false)
      console.log('handle clear completed to dos')
      this.setState({ todos: newCompleted })
  }


  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onKeyDown={this.handleAddTodo}
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleCompleteToDo={this.handleCompleteToDo} />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed"
          onClick= {this.handleClearComplete}
          >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleCompleteToDo}
          />
          <label>{this.props.title}</label>
          <button onClick={this.props.handleDelete}
          className="destroy" />

        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleDelete={(evt) => this.props.handleDelete(todo.id)}
              handleCompleteToDo={(evt) => this.props.handleCompleteToDo(todo.id)} />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;






  // handleSubmit = (value) => {

  // }

  






